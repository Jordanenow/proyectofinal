<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Registro;
use App\Models\Zona;
use DateTime;
use Illuminate\Support\Facades\Auth;

class RegistroController extends Controller
{
    public function getShow($id)
    {
        $Registro = Registro::findOrFail($id);

        return view('registro/show', ['Registro' => $Registro]);
    }
    public function getIndex()
    {
        
        $registro =DB::table("registros")
            ->join('users','registros.user_id','=','users.id')
            ->join('zonas','registros.zona_id','=','zonas.id')
            ->select('registros.*','users.name','zonas.nombre')
            ->paginate(10);
        /*
        $nombre_zona =DB::table('registros')
            ->join('zonas','.registros.zona_id','=','zonas.id')
            ->select('registros.*','zonas.nombre')
            ->get();
      */
        //$Registro = DB::table('registros')->paginate(10);

        return view('registro/index', ['registros' => $registro]);
    }
    public function getCreate()
    {
         //Para coger las zonas
        $zonas = Zona::all();
        return view('registro/create',["zonas"=> $zonas]);
    }
    //public function getEdit($id)
    // {
    //     $Registro = Registro::findOrFail($id);

    //     return view('registro/edit', ['registro' => $Registro]);
    // }
    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear Registro

        $registro = new Registro();
        //Para coger la ide del usuario actual
        Auth::user()->id;
        $id = Auth::user()->id;
        $nombre = DB::table("users")->where("id", $id)->select("name")->get();
    
       

        $registro->hora = new DateTime();
        $registro-> user_id = $id;
        $registro-> zona_id = $request-> input('zonas');

        $registro->save();

        return redirect('registro');
    }

    // public function putEdit(Request $request, $id)
    // {

    //     //----------------------------------------------------------------------------------------
    //     //Editar Registro
    //     $registro = Registro::findorfail($id);
    //     $registro->nombre = $request->input('nombre');
    //     $registro->horario = $request->input('horario');
    //     $registro->save();
    //     return redirect('registro');
    // }
    
    public function putDelete(Request $request, $id)
    {

        $borrar =  Registro::findorfail($id);
        $borrar->delete();
        return redirect('registro');
    }
}
