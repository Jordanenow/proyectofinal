<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Worker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class MasterController extends Controller
{





    public function getShow($id)
    {

        $User = User::findOrFail($id);

        return view('catalogo/show', ['User' => $User]);
    }
    public function getIndex()
    {

        $User = DB::table('users')->paginate(10);

        return view('catalogo/index', ['users' => $User]);
    }
    public function getCreate()
    {

        return view('catalogo/create');
    }
    public function getEdit($id)
    {
        $User = User::findOrFail($id);

        return view('catalogo/edit', ['user' => $User]);
    }
    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear Usuarios

        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return redirect('catalogo');

        //----------------------------------------------------------------------------------------
        /* Crear Trabajadores
        $request -> file('imagen') -> store('public');
        $worker = new Worker();
        $worker -> id = $request -> input('nombre');
        $worker -> nombre = $request -> input('nombre');
       * $worker->validate([

            'imagen' => 'required|mimes:png,jpg|max:2048',
           
           ]);*
       // $worker -> imagen = asset('storage/'. $request->file('imagen')-> hashName());
        $worker -> imagen = $request -> input('imagen');
        $worker -> correo = $request -> input('correo');
        $worker -> save();
        
        return redirect('catalogo');
    */
    }
    public function putEdit(Request $request, $id)
    {

        //----------------------------------------------------------------------------------------
        //Editar User
        $user = User::findorfail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();
        return redirect('catalogo');

        //----------------------------------------------------------------------------------------
        /* Modificar trabajadores
        $worker = Worker::findorfail($id);
        $worker -> id = $request -> input('id');
        $worker -> nombre = $request -> input('nombre');
        //$worker -> fecha_nacimiento = $request -> input('fecha');
        $worker -> correo = $request -> input('correo');
        $worker -> save();
        return redirect('catalogo');
 */
    }
    public function putDelete(Request $request, $id)
    {

        $borrar =  User::findorfail($id);
        $borrar->delete();
        return redirect('catalogo');
        //----------------------------------------------------------------------------------------
        /* Borrar Trabajadores
     $borrar =  Worker::findorfail($id);
     $borrar -> delete();
     return redirect('catalogo');
 */
    }
}
