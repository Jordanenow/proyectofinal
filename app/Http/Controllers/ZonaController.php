<?php

namespace App\Http\Controllers;

use App\Models\Zona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ZonaFormRequest;
class ZonaController extends Controller
{

    public function getShow($id)
    {
        $Zona = Zona::findOrFail($id);

        return view('edificio/show', ['Zona' => $Zona]);
    }
    public function getIndex()
    {

        $Zona = DB::table('zonas')->paginate(10);

        return view('edificio/index', ['zonas' => $Zona]);
    }
    public function getCreate()
    {

        return view('edificio/create');
    }
    public function getEdit($id)
    {
        $Zona = Zona::findOrFail($id);

        return view('edificio/edit', ['zona' => $Zona]);
    }
    public function postCreate(ZonaFormRequest $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear Zonas

        //El validator
        /*$validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'horario' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('edificio/create')
                        ->withErrors($validator)
                        ->withInput();
        }*/
        $zona = new Zona();
        $zona->nombre = $request->input('nombre');
        $zona->horario = $request->input('horario');
        $zona->save();

        return redirect('edificio');
    }
    public function putEdit(ZonaFormRequest $request, $id)
    {

        //----------------------------------------------------------------------------------------
        //Editar Zona
        $zona = Zona::findorfail($id);
        $zona->nombre = $request->input('nombre');
        $zona->horario = $request->input('horario');
        $zona->save();
        return redirect('edificio');
    }
    public function putDelete(Request $request, $id)
    {

        $borrar =  Zona::findorfail($id);
        $borrar->delete();
        return redirect('edificio');
    }
}
