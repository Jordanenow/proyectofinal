@extends('layouts.master2')

@section('content')



<div class="row" style="margin-top:40px">
   <div class="offset-md-3 col-md-6">
      <div class="card">
         <div class="card-header text-center">
            Añadir Trabajador
         </div>
         <div class="card-body" style="padding:30px">

            {{-- TODO: Abrir el formulario e indicar el método POST --}}
            <form action="" method="post" enctype="multipart/form-data">
            {{-- TODO: Protección contra CSRF --}}
            @csrf
           
            @method('POST')



            <label for="name">Nombre</label>
               <input type="text" name="name" id="name" class="form-control" value="">
            </div>

           

           

            <div class="form-group">
               {{-- TODO: Completa el input para el correo --}}
               <label for="email">Correo electronico</label>
               <input type="text" name="email" id="email" class="form-control" value="">
            
            </div>

            <div class="form-group">
               {{-- TODO: Completa el input para la contraseña --}}
               <label for="password">Contraseña</label>
               <input type="text" name="password" id="password" class="form-control" value="">
            
            </div>
           
            <div class="form-group text-center">
               <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                   Crear trabajador
               </button>
            </div>
            
            </form>
            {{-- TODO: Cerrar formulario --}}

         </div>
      </div>
   </div>
</div>
@stop