@extends('layouts.master2')

@section('content')


<div class="row" style="margin-top:40px">
   <div class="offset-md-3 col-md-6">
      <div class="card">
         <div class="card-header text-center">
            Modificar Trabajador
         </div>
         <div class="card-body" style="padding:30px">

            {{-- TODO: Abrir el formulario e indicar el método POST --}}
            <form action="" method="post" enctype="multipart/form-data">
               {{-- TODO: Protección contra CSRF --}}
               @csrf
               @method('PUT')
               <!-- <div class="form-group">
                  

                  <label for="id">ID</label>
                  <input type="text" name="id" id="id" class="form-control" value="{{$user->id}}">
               </div> -->

                  <label for="name">Nombre</label>
                  <input type="text" name="name" id="name" class="form-control" value="{{$user->name}}">
               </div>

            

               

               <div class="form-group">
                  {{-- TODO: Completa el input para el correo --}}
                  <label for="email">Correo electronico</label>
                  <input type="text" name="email" id="email" class="form-control" value="{{$user->email}}">

               </div>

               <div class="form-group text-center">
                  <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                     Modificar Trabajador
                  </button>
               </div>
            </form>
               {{-- TODO: Cerrar formulario --}}

         </div>
      </div>
   </div>
</div>

<form action="/foo/bar" method="POST">
   @method('PUT')

</form>

@stop