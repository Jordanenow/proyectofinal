<!-- FOOTER
================================================== -->
<footer id="footer" class="footer2">
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<small>
					&copy; <a href="https://www.wowthemes.net/">
							  Expertum - Free HTML Template by WowThemes.net
							</a>
					</small>
				</div>
				<div class="col-md-6 text-right">
					<div class="footer-menu">
						<ul id="menu-footer-links" class="menu">
						<li class="nav-item">
                        <form action="{{ url('/logout') }}" method="POST" >
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-link nav-link">
                              Cerrar sesión
                            </button>
                        </form>
                    </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
