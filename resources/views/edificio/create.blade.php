@extends('layouts.master2')

@section('content')


@if ($errors->any())

<div class="row justify-content-center">

   <div class="col-sm-7">

      <div class="alert alert-danger">

         <ul>

            @foreach($errors->all() as $error)

            <li>{{$error}}</li>

            @endforeach

         </ul>

      </div>

   </div>

</div>

@endif

<div class="row" style="margin-top:40px">
   <div class="offset-md-3 col-md-6">
      <div class="card">
         <div class="card-header text-center">
            Añadir Zona
         </div>
         <div class="card-body" style="padding:30px">

            {{-- TODO: Abrir el formulario e indicar el método POST --}}
            <form action="" method="post" enctype="multipart/form-data">
               {{-- TODO: Protección contra CSRF --}}
               @csrf
               @method('POST')



               <label for="nombre">Nombre</label>
               <input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre')}}">
         </div>





         <div class="form-group">
            {{-- TODO: Completa el input para el correo --}}
            <label for="horario">Horario</label>
            <input type="text" name="horario" id="horario" class="form-control" value="{{old('horario')}}">

         </div>

         <div class="form-group text-center">
            <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
               Crear Zona
            </button>
         </div>

         </form>
         {{-- TODO: Cerrar formulario --}}

      </div>
   </div>
</div>
</div>
@stop