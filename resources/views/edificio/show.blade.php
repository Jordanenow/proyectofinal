@extends('layouts.master2')

@section('content')

    <h1>Muestra {{$Zona -> id}}</h1>

    <div class="container">
    <div class="row">



<div class="col-sm-4">


<p>{{$Zona -> horario}}</p>


</div>

<div class="col-sm-8">


<p>{{$Zona -> nombre}}</p>


<a class="btn btn-warning" href= "{{url('edificio/edit/'.$Zona->id)}}" >Editar la Zona</a>
<form method="POST" action="{{url('/edificio/delete').'/'.$Zona->id}}" style="display:inline">

 @method('DELETE')

 @csrf

 <button type="submit" class="btn btn-danger" role="button">

 Borrar

 </button>

</form>
</div>


</div>
    </div>

@stop