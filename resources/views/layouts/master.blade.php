<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
<title>Cleaning.com</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all'/>
<link rel='stylesheet' href='css/animate.min.css' type='text/css' media='all'/>
<link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
<link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/flexslider.css' type='text/css' media='all'/>
</head>

<body class="frontpage">


@include('partials.navbar')


<div class="container">
@yield('content') 

@include('partials.footer')
</div>
<!-- SCRIPTS
================================================== -->
<script src="js/jquery.js"></script>
<script src="js/plugins.js"></script>
<script src="contact/topvalidate.js"></script>
<script src="contact/bottomvalidate.js"></script>

</body>
</html>
