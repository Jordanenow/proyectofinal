@extends('layouts.master')

@section('content')
<h1>Registros</h1>

<div class="row">

 @foreach( $registros as $registro )

 <div class="col-xs-6 col-sm-4 col-md-3 text-center">

<a href="{{ url('/registro/show/' . $registro->id ) }}"> 


 <h4 style="min-height:45px;margin:5px 0 10px 0">

 El usuario {{$registro-> name }}, limpio la zona {{$registro-> nombre }}, el {{$registro->hora }}.

 </h4>

 </a>

 </div>

 @endforeach

 {{$registros->links()}}

</div>



@stop