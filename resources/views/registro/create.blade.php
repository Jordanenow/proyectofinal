@extends('layouts.master2')

@section('content')


<div class="row" style="margin-top:40px">
   <div class="offset-md-3 col-md-6">
      <div class="card">
         <div class="card-header text-center">
            Añadir Registro
         </div>
         <div class="card-body" style="padding:30px">

            {{-- TODO: Abrir el formulario e indicar el método POST --}}
            <form action="" method="post" enctype="multipart/form-data">
               {{-- TODO: Protección contra CSRF --}}
               @csrf
               @method('POST')

               <div class="form-group">
                  <label for="zonas">Zona</label>
                  <select name="zonas" id="zonas">
                  @foreach($zonas as $key => $zona)
                     
                     <option value="{{$zona -> id}}"> {{$zona -> nombre}}</option>
                    
                  @endforeach
                  </select>
               </div>

               <div class="form-group text-center">
                  <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                     Crear Registro
                  </button>
               </div>

            </form>
            {{-- TODO: Cerrar formulario --}}

         </div>
      </div>
   </div>
</div>
@stop