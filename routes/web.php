<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MasterController;
use App\Http\Controllers\ZonaController;
use App\Http\Controllers\RegistroController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('auth/login', function () {
    return view('auth/login');
});
Route::get('auth/logout', function () {
    return'Logout usuario';
});

Route::get('/', [HomeController::class, 'getHome']); 

Route::get('catalogo', [MasterController::class, 'getIndex']); 

Route::get('/index', function () {
    return view('index');
});

//Rutas de los trabajadores

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Auth::routes(['verify' => 'true']);

Route::group(['middleware' => 'verified'], function () {


    Route::get('catalogo/create', [MasterController::class, 'getCreate']); 

    Route::get('catalogo/edit/{id}', [MasterController::class, 'getEdit']); 
    
    Route::get('catalogo/show/{id}', [MasterController::class, 'getShow']);

    
   Route::put('catalogo/edit/{id}',[MasterController::class,'putEdit']);

});

//Route::put('catalogo/edit/{id}',[MasterController::class,'putEdit']);

Route::post('catalogo/create',[MasterController::class,'postCreate']);

Route::delete('/catalogo/delete/{id}',[MasterController::class,'putDelete']);

//---------------------------------------------------------------------------------------
//Rutas de las Zonas

Route::get('edificio', [ZonaController::class, 'getIndex']); 



Auth::routes(['verify' => 'true']);

Route::group(['middleware' => 'verified'], function () {


    Route::get('edificio/create', [ZonaController::class, 'getCreate']); 

    Route::get('edificio/edit/{id}', [ZonaController::class, 'getEdit']); 
    
    Route::get('edificio/show/{id}', [ZonaController::class, 'getShow']);

    
   Route::put('edificio/edit/{id}',[ZonaController::class,'putEdit']);

});


Route::post('edificio/create',[ZonaController::class,'postCreate']);

Route::delete('/edificio/delete/{id}',[ZonaController::class,'putDelete']);

//---------------------------------------------------------------------------------------
//Rutas de las Registros

Route::get('registro', [RegistroController::class, 'getIndex']); 

Auth::routes(['verify' => 'true']);

Route::group(['middleware' => 'verified'], function () {


    Route::get('registro/create', [RegistroController::class, 'getCreate']); 

    Route::get('registro/edit/{id}', [RegistroController::class, 'getEdit']); 
    
    Route::get('registro/show/{id}', [RegistroController::class, 'getShow']);

    
  // Route::put('edificio/edit/{id}',[RegistroController::class,'putEdit']);

});


Route::post('registro/create',[RegistroController::class,'postCreate']);

Route::delete('/registro/delete/{id}',[RegistroController::class,'putDelete']);


